// Christopher Bartos
// 2131542

package Application;

import vehicles.Bicycle;

public class BikeStore {
    public static void main(String [] args) {
        Bicycle[] bikes = new Bicycle[4];

        bikes[0] = new Bicycle("M", 2, 4.00);
        bikes[1] =  new Bicycle("B", 3, 60.00);
        bikes[2] = new Bicycle("E", 7, 200.00);
        bikes[3] =  new Bicycle("Z", 1, 16.00);

        for (Bicycle i: bikes){
            System.out.println(i.toString());
        }
    }
}